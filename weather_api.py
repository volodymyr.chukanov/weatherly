import httpx
from config import Config
from redis_handler import get_redis_client
from datetime import datetime, date
from fastapi import FastAPI, HTTPException, Header
import asyncio
from logging_config import logger

config = Config()
redis_client = get_redis_client()


async def get_city_coords(city_name: str = Header(...)):
    try:
        async with httpx.AsyncClient() as client:
            response = await client.get(
                f"http://api.openweathermap.org/geo/1.0/direct?q={city_name}&limit=1&appid={config.API_KEY}"
            )
            response.raise_for_status()  # Check for HTTP errors
            city_coords = response.json()
            lat, lon = city_coords[0]["lat"], city_coords[0]["lon"]
        return lat, lon
    except Exception as e:
        logger.error(f"Error in get_city_coords: {str(e)}")


async def fetch_temperature():
    try:
        async with httpx.AsyncClient() as client:
            lat, lon = await get_city_coords(config.CITY_NAME)
            response = await client.get(
                f"https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={config.API_KEY}&units=metric"
            )
            response.raise_for_status()  # Check for HTTP errors
            data = response.json()
            temperature = data["main"]["temp"]
        return temperature
    except Exception as e:
        logger.error(f"Error in fetch_temperature: {str(e)}")


async def save_temperature_task():
    try:
        temperature = await fetch_temperature()
        now = datetime.now()
        today_date = now.strftime("%Y-%m-%d")
        current_hour = now.hour
        redis_key = f"{config.CITY_NAME}:{today_date}"
        try:
            redis_client.hset(redis_key, f"{current_hour}", f"{temperature}°C")
            logger.info(
                f"Saved the temperature ({temperature}°C) for {config.CITY_NAME} for this hour: {current_hour}"
            )
        except redis.RedisError as redis_error:
            logger.error(f"Error in Redis operation: {redis_error}")
    except Exception as e:
        logger.error(f"Error in save_temperature: {str(e)}")


async def get_temperature_history(redis_client, date):
    try:
        temperature_history = redis_client.hgetall(f"{config.CITY_NAME}:{date}")
        return temperature_history
    except Exception as e:
        logger.error(f"Error in get_temperature_history: {str(e)}")
        raise HTTPException(status_code=500, detail="Internal Server Error")
