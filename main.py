from config import Config
from redis_handler import get_redis_client
from weather_api import get_temperature_history, save_temperature_task
from scheduler import start_scheduler
from datetime import date
from fastapi import FastAPI, HTTPException, Header, Query, BackgroundTasks
from logging_config import logger

config = Config()
app = FastAPI()
redis_client = get_redis_client()


@app.on_event("startup")
async def startup_event():
    try:
        await start_scheduler()
    except Exception as e:
        logger.error(f"Error during startup: {str(e)}")


@app.get("/temperature", response_model=dict)
async def show_temperature_history(
    date: date = Query(..., title="Date in format Y-m-d"),
    x_token: str = Header(..., title="x-token"),
):
    if x_token != config.X_TOKEN:
        raise HTTPException(status_code=401, detail="Invalid API key")
    try:
        temperature_history = await get_temperature_history(redis_client, date)
        return {"temperature_history": temperature_history}
    except Exception as e:
        logger.error(f"Error while fetching temperature history: {str(e)}")
        raise HTTPException(status_code=500, detail="Internal Server Error")
