# Weatherly

## Overview

This is a simple weather app built with FastAPI and utilizes OpenWeatherMap API for fetching weather data. It fetches the current temperature for a specified city, saves it to a Redis database, and provides a history of temperatures.

## Features

- Fetches current weather data for a specified city
- Stores temperature data in a Redis database
- Provides a history of temperatures based on date
- Scheduled task to update temperature data periodically

## Technologies Used

- FastAPI
- OpenWeatherMap API
- Redis
- Python

## Setup

1. **Clone the repository:**
   git clone https://gitlab.com/volodymyr.chukanov/weatherly.git
   cd weatherly
2. **Create a virtual environment:**
   python -m venv venv
3. **Activate a virtual environment:**
   -Windows:
   venv\Scripts\activate
   -Linux/macOS:
   source venv/bin/activate
4. **Install dependencies:**
   pip install -r requirements.txt
5. **Setup .env file:**
   API_KEY=your_openweathermap_api_key
   X_TOKEN=your_api_token
   CITY_NAME=your_default_city
   REDIS_HOST=your_redis_host
   REDIS_PORT=your_redis_port
   REDIS_DB=your_redis_db
6. **Run the application:**
   uvicorn main:app --reload

## Usage

The app will be accessible at http://127.0.0.1:8000
Open your browser and navigate to http://127.0.0.1:8000/docs for the Swagger documentation.
Use the /temperature endpoint to fetch current weather and historical temperature data.
