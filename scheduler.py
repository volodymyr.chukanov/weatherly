from weather_api import save_temperature_task
from apscheduler.schedulers.asyncio import AsyncIOScheduler
from fastapi import BackgroundTasks


async def start_scheduler():
    # Perform initial fetch and save
    await save_temperature_task()
    scheduler = AsyncIOScheduler()
    scheduler.add_job(save_temperature_task, "interval", seconds=3600)
    scheduler.start()
