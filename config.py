import os
from dotenv import load_dotenv

load_dotenv()


class Config:
    def __init__(self):
        self.API_KEY = os.environ.get("API_KEY")
        self.X_TOKEN = os.environ.get("X_TOKEN")
        self.CITY_NAME = os.environ.get("CITY_NAME")
        self.REDIS_HOST = os.environ.get("REDIS_HOST")
        self.REDIS_PORT = os.environ.get("REDIS_PORT")
        self.REDIS_DB = os.environ.get("REDIS_DB")
