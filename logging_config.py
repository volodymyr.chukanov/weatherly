import logging


def configure_logger():
    logging.basicConfig(level=logging.INFO)


configure_logger()
logger = logging.getLogger(__name__)
